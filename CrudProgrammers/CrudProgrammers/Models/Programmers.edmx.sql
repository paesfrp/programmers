
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 03/11/2019 15:22:35
-- Generated from EDMX file: E:\Pessoal\bitbucket\Programmers\programmers\CrudProgrammers\CrudProgrammers\Models\Programmers.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Programmers];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[TB_USUARIO]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TB_USUARIO];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'TB_USUARIO'
CREATE TABLE [dbo].[TB_USUARIO] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [NOME_COMPLETO] nvarchar(50)  NULL,
    [EMAIL] nvarchar(50)  NULL,
    [TELEFONE] nvarchar(10)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID] in table 'TB_USUARIO'
ALTER TABLE [dbo].[TB_USUARIO]
ADD CONSTRAINT [PK_TB_USUARIO]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------