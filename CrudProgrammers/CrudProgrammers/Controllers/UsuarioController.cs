﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CrudProgrammers.Models;

namespace CrudProgrammers.Controllers
{
    public class UsuarioController : Controller
    {
        private ProgrammersEntities1 db = new ProgrammersEntities1();

        // GET: Usuario
        public ActionResult Index(string NomeUsuario)
        {
            var usuario = new List<TB_USUARIO>();
            if (!string.IsNullOrEmpty(NomeUsuario))
            {
                usuario = db.TB_USUARIO.Where(x => x.NOME_COMPLETO.Contains(NomeUsuario)).ToList();
            }
            else
            {
                usuario = db.TB_USUARIO.ToList();
            }
            return View(usuario);
        }

        // GET: Usuario/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TB_USUARIO tB_USUARIO = db.TB_USUARIO.Find(id);
            if (tB_USUARIO == null)
            {
                return HttpNotFound();
            }
            return View(tB_USUARIO);
        }

        // GET: Usuario/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Usuario/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,NOME_COMPLETO,EMAIL,TELEFONE")] TB_USUARIO tB_USUARIO)
        {
            if (ModelState.IsValid)
            {
                tB_USUARIO.TELEFONE = tB_USUARIO.TELEFONE.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ","").Trim();
                db.TB_USUARIO.Add(tB_USUARIO);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tB_USUARIO);
        }

        // GET: Usuario/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TB_USUARIO tB_USUARIO = db.TB_USUARIO.Find(id);
            if (tB_USUARIO == null)
            {
                return HttpNotFound();
            }
            return View(tB_USUARIO);
        }

        // POST: Usuario/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,NOME_COMPLETO,EMAIL,TELEFONE")] TB_USUARIO tB_USUARIO)
        {
            if (ModelState.IsValid)
            {
                tB_USUARIO.TELEFONE = tB_USUARIO.TELEFONE.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim();
                db.Entry(tB_USUARIO).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tB_USUARIO);
        }

        // GET: Usuario/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TB_USUARIO tB_USUARIO = db.TB_USUARIO.Find(id);
            if (tB_USUARIO == null)
            {
                return HttpNotFound();
            }
            return View(tB_USUARIO);
        }

        // POST: Usuario/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TB_USUARIO tB_USUARIO = db.TB_USUARIO.Find(id);
            db.TB_USUARIO.Remove(tB_USUARIO);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public string FormatTEL(string TEL)
        {
            string tel = string.Empty;
            if (TEL.Length <= 10)
                tel = Convert.ToUInt64(TEL).ToString("(##) ####-####");
            else if (TEL.Length > 10)
                tel = Convert.ToUInt64(TEL).ToString("(##) #####-####");

            return tel;
        }
    }
}
