﻿using System.Web;
using System.Web.Optimization;

namespace CrudProgrammers
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"
                        , "~/Scripts/jquery.mask.js"
                        , "~/Scripts/jquery.mask.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/vendor/bootstrap/css/bootstrap.min.css",
                      "~/vendor/fontawesome-free/css/all.min.css",
                      "~/vendor/datatables/dataTables.bootstrap4.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/Usuariocss").Include(
                        "~/Content/bootstrap.css"
                      ,"~/Content/site.css"
                      ,"~/vendor/bootstrap/css/bootstrap.min.css"
                      
                      ));

            bundles.Add(new ScriptBundle("~/bundles/Usuariojs").Include(
                "~/vendor/bootstrap/js/bootstrap.bundle.min.js"
                ,"~/vendor/bootstrap/js/bootstrap.js"
                , "~/vendor/datatables/jquery.dataTables.js"
                , "~/vendor/datatables/dataTables.bootstrap4.js"
               
                ));
        }
    }
}
